## Installation

### Sublime Text 2
```bash
cd ~/Library/Application\ Support/Sublime\ Text\ 2/Installed\ Packages
git clone https://haines@bitbucket.org/haines/sublime-hybris.git hybris
```

### Sublime Text 3
```bash
cd ~/Library/Application\ Support/Sublime\ Text\ 3/Packages
git clone https://haines@bitbucket.org/haines/sublime-hybris.git hybris
```
